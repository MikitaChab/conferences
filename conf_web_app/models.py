# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Events(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.TextField()
    location_id = models.IntegerField()
    start_date = models.TextField()
    end_date = models.TextField()

    class Meta:
        managed = False
        db_table = 'events'


class Locations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.TextField(unique=True)
    capacity = models.IntegerField()
    address = models.TextField()

    class Meta:
        managed = False
        db_table = 'locations'


class Orators(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    first_name = models.TextField()
    last_name = models.TextField()

    class Meta:
        managed = False
        db_table = 'orators'


class Participants(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user_id = models.IntegerField()
    event_id = models.IntegerField()
    active = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'participants'


class Perfomances(models.Model):
    orator_id = models.IntegerField()
    event_id = models.IntegerField()
    theme = models.TextField()
    id = models.IntegerField(primary_key=True)  # AutoField?

    class Meta:
        managed = False
        db_table = 'perfomances'


class Sponsors(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.TextField()
    nip = models.IntegerField(unique=True, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sponsors'


class Sponsorships(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    event_id = models.IntegerField()
    sponsor_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'sponsorships'


class Users(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nikname = models.TextField(unique=True)
    first_name = models.TextField()
    last_name = models.TextField()
    email = models.TextField()

    class Meta:
        managed = False
        db_table = 'users'
