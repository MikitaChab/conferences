from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone


class Location(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.TextField(unique=True)
    capacity = models.IntegerField()
    address = models.TextField()

    def __str__(self):
        return self.name + ', ' + self.address

    class Meta:
        managed = False
        db_table = 'locations'


class Event(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=200)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    start_date = models.CharField(max_length=50)
    end_date = models.CharField(max_length=50)
    content = models.CharField(max_length=1000)
    organizer = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        managed = False
        db_table = 'events'

    def get_absolute_url(self):
        return reverse('event-detail', kwargs={'pk': self.pk})


class Orator(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        managed = False
        db_table = 'orators'


class Participant(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    active = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.user} - {self.event.name}'

    class Meta:
        managed = False
        db_table = 'participants'


class Perfomance(models.Model):
    orator = models.ForeignKey(Orator, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    theme = models.CharField(max_length=200)
    id = models.IntegerField(primary_key=True)  # AutoField?

    class Meta:
        managed = False
        db_table = 'perfomances'

    def __str__(self):
        return self.orator.__str__() + " - " + self.theme


class Sponsor(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=100)
    nip = models.IntegerField(unique=True, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sponsors'

    def __str__(self):
        return self.name


class Sponsorship(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    sponsor = models.ForeignKey(Sponsor, on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'sponsorships'

    def __str__(self):
        return self.event.__str__() + " " + self.sponsor.__str__()


