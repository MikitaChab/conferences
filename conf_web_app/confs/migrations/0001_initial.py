# Generated by Django 2.1.4 on 2018-12-24 16:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Events',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.TextField()),
                ('location_id', models.IntegerField()),
                ('start_date', models.TextField()),
                ('end_date', models.TextField()),
            ],
            options={
                'db_table': 'events',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Locations',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.TextField(unique=True)),
                ('capacity', models.IntegerField()),
                ('address', models.TextField()),
            ],
            options={
                'db_table': 'locations',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Orators',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('first_name', models.TextField()),
                ('last_name', models.TextField()),
            ],
            options={
                'db_table': 'orators',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Participants',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('user_id', models.IntegerField()),
                ('event_id', models.IntegerField()),
                ('active', models.IntegerField()),
            ],
            options={
                'db_table': 'participants',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Perfomances',
            fields=[
                ('orator_id', models.IntegerField()),
                ('event_id', models.IntegerField()),
                ('theme', models.TextField()),
                ('id', models.IntegerField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'perfomances',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Sponsors',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.TextField()),
                ('nip', models.IntegerField(blank=True, null=True, unique=True)),
            ],
            options={
                'db_table': 'sponsors',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Sponsorships',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('event_id', models.IntegerField()),
                ('sponsor_id', models.IntegerField()),
            ],
            options={
                'db_table': 'sponsorships',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('nikname', models.TextField(unique=True)),
                ('first_name', models.TextField()),
                ('last_name', models.TextField()),
                ('email', models.TextField()),
            ],
            options={
                'db_table': 'users',
                'managed': False,
            },
        ),
    ]
