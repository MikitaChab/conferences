# Generated by Django 2.1.4 on 2019-01-03 15:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('confs', '0002_event_location_orator_participant_perfomance_post_sponsor_sponsorship'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='date_posted',
        ),
    ]
