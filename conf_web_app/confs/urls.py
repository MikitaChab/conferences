from django.urls import path
from . import views
from .views import EventListView, EventDetailView, EventCreateView, EventUpdateView, \
    EventDeleteView, PerfomanceCreateView, SponsorshipCreateView, ParticipantCreateView, UserEventsListView, \
    SponsorCreateView, OratorCreateView, ParticipantDeleteView, ParticipantListView, SignUpAsSpeaker, \
    PerfomanceListView, UserByEventsView

urlpatterns = [
    path('', EventListView.as_view(), name='confs-home'),
    path('my-tickets', UserEventsListView.as_view(), name='my-tickets'),
    path('my-confs', UserByEventsView.as_view(), name='my-confs'),
    path('my-perfs', PerfomanceListView.as_view(), name='my-perfs'),
    path('about', views.about, name='confs-about'),
    path('event/<int:pk>/', EventDetailView.as_view(), name='event-detail'),
    path('event/<int:pk>/participants', ParticipantListView.as_view(), name='event-participants'),
    path('event/<int:pk>/update', EventUpdateView.as_view(), name='event-update'),
    path('event/<int:pk>/delete', EventDeleteView.as_view(), name='event-delete'),
    path('event/<int:pk>/add-speaker', PerfomanceCreateView.as_view(), name='event-add-speaker'),
    path('event/<int:pk>/add-sponsor/', SponsorshipCreateView.as_view(), name='event-add-sponsor'),
    path('event/<int:pk>/signup', ParticipantCreateView.as_view(), name='event-signup'),
    path('event/<int:pk>/signup-speaker', SignUpAsSpeaker.as_view(), name='event-signup-speaker'),
    path('event/<int:pk>/cancel', ParticipantDeleteView.as_view(), name='event-cancel'),
    path('<int:pk>/cancel', ParticipantDeleteView.as_view(), name='ticket-cancel'),
    path('new-sponsor', SponsorCreateView.as_view(), name='new-sponsor'),
    path('new-orator', OratorCreateView.as_view(), name='new-orator'),
    path('event/new/', EventCreateView.as_view(), name='event-create'),
]
