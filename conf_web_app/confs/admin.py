from django.contrib import admin
from .models import Event, Location, Orator, Participant,\
                            Perfomance,Sponsor,Sponsorship

admin.site.register(Event)
admin.site.register(Location)
admin.site.register(Orator)
admin.site.register(Participant)
admin.site.register(Perfomance)
admin.site.register(Sponsorship)
admin.site.register(Sponsor)
