from django import template

register = template.Library()


@register.filter()
def event_filter(perfs, event):
    return perfs.filter(event=event)

