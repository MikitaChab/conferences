from django.test import TestCase

# Create your tests here.
from .models import Event, Location, Orator, Perfomance, Participant, Sponsor, Sponsorship
from django.contrib.auth.models import User


class LocationTestCase(TestCase):

    def setUp(self):
        self.location_dict = {
            'name': 'test location',
            'capacity': 1000,
            'address': 'test address'
        }
        Location.objects.create(**self.location_dict)
        self.location = Location.objects.filter(name='test location').first()

    def test_location_created(self):
        self.assertTrue(self.location)

    def test_location_name(self):
        self.assertEqual(self.location.name, self.location_dict['name'])

    def test_location_capacity(self):
        self.assertEqual(self.location.capacity, self.location_dict['capacity'])

    def test_location_address(self):
        self.assertEqual(self.location.address, self.location_dict['address'])

    def test_location_delet(self):
        self.location.delete()


class EventTestCase(TestCase):

     def setUp(self):
         self.organizer = User.objects.get(id=1)
         self.location_dict = {
             'name': 'test location',
             'capacity': 1000,
             'address': 'test address'
         }
         Location.objects.create(**self.location_dict)
         self.location = Location.objects.filter(**self.location_dict).first()
         self.event_dict = {
             'name': 'test conf',
             'content': 'test content',
             'start_date': '01-01-2019',
             'end_date': '02-01-2019',
             'location': self.location,
             'organizer': self.organizer
          }
         Event.objects.create(**self.event_dict)
         self.event = Event.objects.filter(name=self.event_dict['name']).first()

     def test_event_created(self):
         self.assertTrue(self.event)

     def test_event_name(self):
        self.assertEqual(self.event.name, self.event_dict['name'])

     def test_event_content(self):
        self.assertEqual(self.event.content, self.event_dict['content'])

     def test_event_start_date(self):
         self.assertEqual(self.event.start_date, self.event_dict['start_date'])

     def test_event_end_date(self):
         self.assertEqual(self.event.end_date, self.event_dict['end_date'])

     def test_event_location(self):
         self.assertEqual(self.event.location.name, self.location_dict['name'])

     def test_event_organizer(self):
         self.assertEqual(self.event.organizer.username, self.organizer.username)

     def test_event_str(self):
         self.assertEqual(self.event.name, str(self.event))


class OratorTestCase(TestCase):

    def setUp(self):
        self.organizer = User.objects.get(id=1)
        self.location_dict = {
            'name': 'test location',
            'capacity': 1000,
            'address': 'test address'
        }
        Location.objects.create(**self.location_dict)
        self.location = Location.objects.filter(**self.location_dict).first()
        self.event_dict = {
            'name': 'test conf',
            'content': 'test content',
            'start_date': '01-01-2019',
            'end_date': '02-01-2019',
            'location': self.location,
            'organizer': self.organizer
        }
        Event.objects.create(**self.event_dict)
        self.event = Event.objects.filter(name=self.event_dict['name']).first()
        self.orator_dict = {
            'first_name': 'test name',
            'last_name': 'test last',

        }
        Orator.objects.create(**self.orator_dict)
        self.orator = Orator.objects.filter(first_name=self.orator_dict['first_name']).first()

    def test_orotar_create(self):
        self.assertTrue(self.orator)

    def test_orator_first_name(self):
        self.assertEqual(self.orator.first_name, self.orator_dict['first_name'])

    def test_orator_last_name(self):
        self.assertEqual(self.orator.last_name, self.orator_dict['last_name'])


class PerfomanceTestCase(TestCase):

    def setUp(self):
        self.organizer = User.objects.get(id=1)
        self.location_dict = {
            'name': 'test location',
            'capacity': 1000,
            'address': 'test address'
        }
        Location.objects.create(**self.location_dict)
        self.location = Location.objects.filter(**self.location_dict).first()
        self.event_dict = {
            'name': 'test conf',
            'content': 'test content',
            'start_date': '01-01-2019',
            'end_date': '02-01-2019',
            'location': self.location,
            'organizer': self.organizer
        }
        Event.objects.create(**self.event_dict)
        self.event = Event.objects.filter(name=self.event_dict['name']).first()
        self.orator_dict = {
            'first_name': 'test name',
            'last_name': 'test last',

        }
        Orator.objects.create(**self.orator_dict)
        self.orator = Orator.objects.filter(first_name=self.orator_dict['first_name']).first()
        self.perf_dict = {
            'event': self.event,
            'orator':self.orator,
            'theme': 'test theme',
        }
        Perfomance.objects.create(**self.perf_dict)
        self.perf = Perfomance.objects.filter(**self.perf_dict).first()

    def test_perf_create(self):
        self.assertTrue(self.perf)

    def test_perf_orator_not_null(self):
        self.assertTrue(self.perf.orator)

    def test_perf_event_not_null(self):
        self.assertTrue(self.perf.event)

    def test_perf_theme(self):
        self.assertEqual(self.perf.theme, self.perf_dict['theme'])


class ParticipantTestCase(TestCase):

    def setUp(self):
        self.organizer = User.objects.get(id=1)
        self.location_dict = {
            'name': 'test location',
            'capacity': 1000,
            'address': 'test address'
        }
        Location.objects.create(**self.location_dict)
        self.location = Location.objects.filter(**self.location_dict).first()
        self.event_dict = {
            'name': 'test conf',
            'content': 'test content',
            'start_date': '01-01-2019',
            'end_date': '02-01-2019',
            'location': self.location,
            'organizer': self.organizer
        }
        Event.objects.create(**self.event_dict)
        self.event = Event.objects.filter(name=self.event_dict['name']).first()
        self.new_user_dict = {
                'username':'test',
                'password':'testpass'
        }
        User.objects.create(**self.new_user_dict)
        self.new_user = User.objects.filter(username=self.new_user_dict['username']).first()
        self.user = self.organizer
        self.participant_dict = {
            'user': self.user,
            'event': self.event,
            'active': False
        }
        Participant.objects.create(**self.participant_dict)
        self.participant = Participant.objects.filter(**self.participant_dict).first()

    def test_user_create(self):
        self.assertTrue(self.new_user)

    def test_event(self):
        self.assertTrue(self.event)

    def test_participant_create(self):
        self.assertTrue(self.participant)

    def test_participant_event(self):
        self.assertTrue(self.participant.event)
        self.assertEqual(self.participant.event.name, self.event_dict['name'])

    def test_participant_active(self):
        self.assertEqual(self.participant.active, self.participant_dict['active'])


class SponsorTestCase(TestCase):

    def setUp(self):
        self.organizer = User.objects.get(id=1)
        self.location_dict = {
            'name': 'test location',
            'capacity': 1000,
            'address': 'test address'
        }
        Location.objects.create(**self.location_dict)
        self.location = Location.objects.filter(**self.location_dict).first()
        self.event_dict = {
            'name': 'test conf',
            'content': 'test content',
            'start_date': '01-01-2019',
            'end_date': '02-01-2019',
            'location': self.location,
            'organizer': self.organizer
        }
        Event.objects.create(**self.event_dict)
        self.event = Event.objects.filter(name=self.event_dict['name']).first()
        self.sponsor_dict  = {
            'name': 'test sponsor',
            'nip': 12345678,
        }
        Sponsor.objects.create(**self.sponsor_dict)
        self.sponsor = Sponsor.objects.filter(**self.sponsor_dict).first()

    def test_sponsor_create(self):
        self.assertTrue(self.sponsor)

    def test_sponsor_name(self):
        self.assertEqual(self.sponsor.name, self.sponsor_dict['name'])

    def test_sponsor_nip(self):
        self.assertEqual(self.sponsor.nip, self.sponsor_dict['nip'])


class SponsorshipTestCase(TestCase):

    def setUp(self):
        self.organizer = User.objects.get(id=1)
        self.location_dict = {
            'name': 'test location',
            'capacity': 1000,
            'address': 'test address'
        }
        Location.objects.create(**self.location_dict)
        self.location = Location.objects.filter(**self.location_dict).first()
        self.event_dict = {
            'name': 'test conf',
            'content': 'test content',
            'start_date': '01-01-2019',
            'end_date': '02-01-2019',
            'location': self.location,
            'organizer': self.organizer
        }
        Event.objects.create(**self.event_dict)
        self.event = Event.objects.filter(name=self.event_dict['name']).first()
        self.sponsor_dict = {
            'name': 'test sponsor',
            'nip': 12345678,
        }
        Sponsor.objects.create(**self.sponsor_dict)
        self.sponsor = Sponsor.objects.filter(**self.sponsor_dict).first()
        self.sponsorship_dict = {
            'event':self.event,
            'sponsor':self.sponsor
        }
        Sponsorship.objects.create(**self.sponsorship_dict)
        self.sponsorship = Sponsorship.objects.filter(**self.sponsorship_dict).first()

    def test_sponsorship_create(self):
        self.assertTrue(self.sponsorship_dict)

    def test_sponsorship_event(self):
        self.assertTrue(self.sponsorship.event)
        self.assertEqual(self.sponsorship.event.name, self.event_dict['name'])

    def test_sponsorship_sponsor(self):
        self.assertTrue(self.sponsorship.sponsor)
        self.assertEqual(self.sponsorship.sponsor.name, self.sponsor_dict['name'])

