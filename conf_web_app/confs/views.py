from django.shortcuts import render, redirect, reverse
from .models import Perfomance, Sponsorship, Event, Participant,  Sponsor, Orator
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from django.forms import HiddenInput
from .forms import OratorForm, PeformanceFrom




class EventListView(ListView):
    model = Event
    template_name = 'confs/home.html'
    context_object_name = 'events'

    # add e xtra context to template
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['performances'] = Perfomance.objects.all()
        context['sponsorships'] = Sponsorship.objects.all()
        return context


class EventDetailView(DetailView):
    model = Event

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['performances'] = Perfomance.objects.all()
        context['sponsorships'] = Sponsorship.objects.all()
        context['participant'] = Participant.objects.filter(event_id=self.kwargs['pk'],
                                                            user_id=self.request.user).first()
        return context


class EventDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Event
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.organizer:
            return True
        return False


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    fields = ['name', 'content', 'location', 'start_date', 'end_date']
    success_url = '/'

    def form_valid(self, form):
        form.instance.organizer = self.request.user
        
        return super().form_valid(form)


class EventUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Event
    fields = ['name', 'location', 'start_date', 'end_date']

    def form_valid(self, form):
        form.instance.organizer = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.organizer:
            return True
        return False

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['event'] = Event.objects.filter(id=self.get_object().id).first()
        context['event_fields'] = ['name']
        return context


class PerfomanceCreateView(UserPassesTestMixin, LoginRequiredMixin, CreateView):
    model = Perfomance
    fields = ['orator', 'theme']

    def get_success_url(self):
        return reverse('event-detail', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        form.instance.event_id = self.kwargs['pk']
        return super().form_valid(form)

    def test_func(self):
        event = Event.objects.filter(id=self.kwargs['pk']).first()
        user = self.request.user
        participant = Participant.objects.filter(event_id=event.id, user_id=user.id).first()
        if user == event.organizer or participant:
            return True
        return False


class PerfomanceListView(LoginRequiredMixin, ListView):
    model = Perfomance

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        first = self.request.user.profile.first_name
        last = self.request.user.profile.last_name
        orator = Orator.objects.filter(first_name=first, last_name=last).first()
        context['performances'] = Perfomance.objects.filter(orator_id = orator.id).all()
        return contextself



class SponsorshipCreateView(UserPassesTestMixin, LoginRequiredMixin, CreateView):
    model = Sponsorship
    fields = ['sponsor']

    def get_success_url(self):
        return reverse('event-detail', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        form.instance.event_id = self.kwargs['pk']
        return super().form_valid(form)

    def test_func(self):
        if self.request.user == Event.objects.filter(id=self.kwargs['pk']).first().organizer:
            return True
        return False


class OratorCreateView(LoginRequiredMixin, CreateView):
    model = Orator
    fields = ['first_name', 'last_name']

    def get_success_url(self):
        return self.request.POST.get('next', '/')

    def form_valid(self, form):
        return super().form_valid(form)


class SponsorCreateView(LoginRequiredMixin, CreateView):
    model = Sponsor
    fields = ['name', 'nip']

    def get_success_url(self):
        return self.request.POST.get('next', '/')

    def form_valid(self, form):
        return super().form_valid(form)


class UserEventsListView(LoginRequiredMixin, ListView):
    model = Event
    template_name = 'confs/my-tickets.html'
    context_object_name = 'events'

    # add extra context to template
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['tickets'] = self.request.user.participant_set.all()
        context['performances'] = Perfomance.objects.all()
        context['sponsorships'] = Sponsorship.objects.all()
        return context


class UserByEventsView(LoginRequiredMixin, ListView):
    model = Event
    template_name = 'confs/my-confs.html'
    context_object_name = 'events'

    # add extra context to template
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['events'] = Event.objects.filter(organizer=self.request.user).all
        context['performances'] = Perfomance.objects.all()
        context['sponsorships'] = Sponsorship.objects.all()
        return context


class ParticipantCreateView(LoginRequiredMixin, CreateView):
    model = Participant
    fields = ['active']

    def get_success_url(self):
        participant = Participant.objects.filter(user_id=self.request.user.id,
                                                 event_id=self.kwargs['pk']).first()
        if participant.active:
            return reverse('event-signup-speaker', kwargs={'pk': self.kwargs['pk']})
        else:
            return '/'

    def form_valid(self, form):
        form.instance.event_id = Event.objects.filter(id=self.kwargs['pk']).first().id
        form.instance.user = self.request.user
        return super().form_valid(form)


class ParticipantDeleteView(LoginRequiredMixin, DeleteView):
    model = Participant

    success_url = '/'


class ParticipantListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Participant

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['participants'] = Participant.objects.filter(event_id = self.kwargs['pk'])
        return context

    def test_func(self):
        if self.request.user == Event.objects.filter(id=self.kwargs['pk']).first().organizer:
            return True
        return False


class SignUpAsSpeaker(LoginRequiredMixin, CreateView):
    model = Perfomance
    fields = ['theme']
    template_name = 'confs/speaker-sign-up.html'

    def get_success_url(self):
        return reverse('event-detail', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        first = self.request.user.profile.first_name
        last = self.request.user.profile.last_name
        orator = Orator.objects.filter(first_name=first, last_name=last).first()
        if not orator:
            Orator.objects.create(first_name=first, last_name=last)
            orator = Orator.objects.filter(first_name=first, last_name=last).first()
            print('orator id', orator.id)
        form.instance.orator_id = orator.id
        form.instance.event_id = self.kwargs['pk']
        return super().form_valid(form)


def about(request):
    return render(request, 'confs/about.html', {'title': 'About'})

