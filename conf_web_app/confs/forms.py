from django import forms
from .models import Perfomance, Orator


class PeformanceFrom(forms.ModelForm):
    class Meta:
        model = Perfomance
        fields = ['theme']


class OratorForm(forms.ModelForm):
    class Meta:
        model = Orator
        fields = ['first_name', 'last_name']
