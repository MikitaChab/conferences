from django.test import TestCase

from .models import Profile
from django.contrib.auth.models import User

# class ProfileTestCase(TestCase):
#
#     def test_profile(self):
#         profile = Profile.objects.filter(first_name='Mikita').first()
#         self.assertEqual(profile.first_name, 'Mikita')


class UserTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.get(id=1)

    def test_user(self):
        self.assertEqual(self.user.username, 'mikita')

    def test_user_admin(self):
        self.assertTrue(self.user.is_staff)

    def test_user_profile_exist(self):
        self.assertTrue(self.user.profile)

    def test_user_profile_last_name(self):
        self.assertEqual(self.user.profile.last_name, 'Chabatarovich')
